# classless.css

Simple miniature UI (almost) classless CSS "framework". It includes `normalize.css` reset and really basic flex based responsive grid system.

## See exmple here: [http://classless.vojtechklos.com/](http://classless.vojtechklos.com//)

## Intro

When I am building some quick mockup or just experimenting or just making something that does not need to be designed but I want it to look at least relatively good I always needed to include Bootstrap or something similar (I know there are much smaller libraries, read onwards). With this type of framework there is almost unlimited posibilities and although I know Bootstrap really well, I sometimes do not feel like adding classes to everything and want to focus on simply writing the interesting part and leaving the boring part that i already wrote a thousand times (CSS Framework classes). Thus I created classless.css.

## What do you mean "classless"?

I mean you write HTML tags without classes and it looks good! Seriously. It is easily customizable because you can just add a class to any element and overwrite the default values.

## But there are classes

Yes we have 4 classes. `is-valid` and `is-error` for form inputs and then `container` and `grid` for the grid system.

`is-valid` and `is-error` give green/red border to form inputs.

`container` class is there to restrict and center width of the webpage.

`grid` sets up a flex container and if you put `div`s inside they will act like columns and will have basic responsivness for mobile devices.